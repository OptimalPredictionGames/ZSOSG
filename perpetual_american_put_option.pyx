"""
# some calculations related to the analytical solution of perpetual delta-callable put option
# obtained from Kyprianou, A. (2004). "Some calculations for Israeli options". Finance and Stochastics, 8(1), pp. 73--86.
"""

from common_imports import *
from scipy.optimize import root as findroot
cimport cython

# the critical value for the writer's penalty, beyond which it is never optimal to exercise.

def getCriticalPenaltyValue(float interest_rate, float volatility, float strike):
    cdef float gamma = (interest_rate / (volatility**2)) + 0.5
    cdef float delta_star = (strike / (2.0 * gamma)) * ( ( ((2.0 * gamma) - 1.0) / (2.0 * gamma))**((2.0 * gamma) - 1.0))
    return delta_star

def getPerpetualAmericanPutOptionValue(float interest_rate, float volatility, float strike, float initial_asset_price):
    cdef float s_star = strike / (1.0 + ((volatility**2) / (2.0 * interest_rate)))
    cdef float option_value

    if initial_asset_price <= s_star:
        option_value = strike - initial_asset_price
    else:
        option_value = (strike - s_star) * ((s_star / initial_asset_price)**((2.0 * interest_rate)/(volatility**2)))

    return option_value

cdef nonLinearEquationForCriticalStrikeValue(float guess, float gamma, float strike, float penalty):
    cdef float LHS = (guess**(2.0 * gamma)) + (2.0 * gamma) - 1.0
    cdef float RHS = 2.0 * gamma * (1.0 + (penalty / strike)) * guess
    cdef float return_value = LHS - RHS
    return return_value

def getCriticalStrikeValue(float interest_rate, float volatility, float strike, float penalty):
    cdef float gamma = (interest_rate / (volatility**2)) + 0.5
    cdef float init = 1.0
    cdef float dec = 0.05
    cdef float k_star
    cdef bint flag = True
    cdef int num_iterations = 100
    cdef int iter = 0

    while flag:
        iter += 1
        k_star = findroot(nonLinearEquationForCriticalStrikeValue, init, args=(gamma, strike, penalty)).x[0]
        if (k_star <= 0.0) or (k_star >= 1.0):
            init -= dec
        else:
            flag = False
        if flag and (iter >= num_iterations):
            flag = False
    print "k_star * strike = %f, F(k_star) = %f" % (k_star * strike, nonLinearEquationForCriticalStrikeValue(k_star, gamma, strike, penalty))
    return k_star * strike

def getDeltaPerpetualAmericanPutOptionValue(float interest_rate, float volatility, float strike, float penalty,
                                             float initial_asset_price):
    cdef float option_value, k_star, gamma, delta_star
    if initial_asset_price <= 0.0:
        option_value = 0.0
    else:
        delta_star = getCriticalPenaltyValue(interest_rate, volatility, strike)
        print "The value of delta_star is %f " % delta_star
        if penalty >= delta_star:
            option_value = getPerpetualAmericanPutOptionValue(interest_rate, volatility, strike, initial_asset_price)
        else:
            if initial_asset_price >= strike:
                gamma = (interest_rate / (volatility**2)) + 0.5
                option_value = penalty * ((initial_asset_price / strike)**(-((2.0 * gamma) - 1.0)))
            else:
                k_star = getCriticalStrikeValue(interest_rate, volatility, strike, penalty)
                if initial_asset_price <= k_star:
                    option_value = strike - initial_asset_price
                else:
                    gamma = (interest_rate / (volatility**2)) + 0.5
                    option_value = ((strike - k_star) * ((initial_asset_price/k_star)**(-(gamma - 1.0))) *
                                    ((((initial_asset_price/strike)**gamma) - ((initial_asset_price/strike)**(-gamma))) /
                                 (((k_star/strike)**gamma) - ((k_star/strike)**(-gamma)))))

                    option_value += (penalty * ((initial_asset_price/strike)**(-(gamma - 1.0))) *
                                 ((((initial_asset_price/k_star)**(-gamma)) - ((initial_asset_price/k_star)**gamma)) /
                                 (((k_star/strike)**gamma) - ((k_star/strike)**(-gamma)))))

    return option_value






