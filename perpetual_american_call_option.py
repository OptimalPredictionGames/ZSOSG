__author__ = 'rmartyr'

from common_imports import *

"""
Taken from Ekstrom, E. and Villeneuve, S. (2006). "On the value of optimal stopping games". The Annals of Applied Probability, 16(3), pp. 1576-1596.
p. 1594
"""

def getValuePerpetualGameCallOptionWithoutDividends(initial_asset_price, strike, penalty):
    if initial_asset_price <= 0:
        option_value = 0.0
    elif initial_asset_price <= strike:
        option_value = penalty * initial_asset_price / strike
    else:
        option_value = initial_asset_price - strike + penalty

    return option_value