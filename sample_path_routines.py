"""
import numpy as np
import math
import pylab as plt

"""

from common_imports import *

def printSamplePaths(sample_paths,initial_time,terminal_time,dt):
    # plot sample paths
    plt.figure()
    x_range = np.arange(0.0,terminal_time - initial_time+dt,dt)
    for i in xrange(sample_paths.shape[0]):
        plt.plot(x_range,sample_paths[i,:])
    plt.xlabel('Time - (t)')
    plt.ylabel('Energy Unit Price - X(t)')
    plt.legend(loc='lower right') 
    plt.title('Sample paths')   
    plt.show()
    
jump_term = lambda mu, sigma, count_term, gaussian_term : (mu * np.ravel(count_term)) + (sigma * np.sqrt(np.ravel(count_term)) * np.ravel(gaussian_term))

# Exact simulation of compound Poisson process (Glasserman p. 138--139)
def oneStepExactSimulationCPP(initial_values,mu,sigma,count_term,gaussian_term):
    
    new_values = np.ravel(initial_values) + jump_term(mu,sigma,count_term,gaussian_term)
    
    return new_values[:,np.newaxis]

def generatePathsCPP(initial_value,rate,mu,sigma,dt,num_time_steps,num_of_paths):
    
    sample_paths = initial_value * np.ones((num_of_paths,1))
    
    std_dev = sigma * (dt ** 0.5)
    
    for i in xrange(num_time_steps):
        
        # generate Poisson random variables
        count_term = np.random.poisson(rate*dt,(num_of_paths,1))
    
        gaussian_term = np.random.normal(0.0,1.0,(num_of_paths,1))
    
        sample_paths = np.hstack([sample_paths,oneStepExactSimulationCPP(sample_paths[:,i],mu,std_dev,count_term,gaussian_term)])
    
    return sample_paths
    
# GBM, p. 94 of Glasserman
    
def generatePathsGBM(initial_value,mean_norm,std_dev_norm,num_time_steps,num_of_paths):
    
    rand_mat = np.random.normal(0.0,1.0,(num_of_paths,num_time_steps))
    
    sample_paths = initial_value * np.hstack([np.ones((num_of_paths,1)),np.cumprod(np.exp(mean_norm + (std_dev_norm * rand_mat)),axis=1)])
    
    return sample_paths
    
def generatePathsGBMAntiThetic(initial_value,mean_norm,std_dev_norm,num_time_steps,num_of_paths):
    
    rand_mat = np.random.normal(0.0,1.0,(num_of_paths/2,num_time_steps))
    
    sample_paths = initial_value * np.hstack([np.ones((num_of_paths,1)),np.cumprod(np.exp(mean_norm + (std_dev_norm * np.vstack([rand_mat,-1.0 * rand_mat]))),axis=1)])
    
    return sample_paths
    
# Exact simulation of OU process Glasserman p.110
def oneStepExactSimulationOU(initial_values,mu_r,theta,sigma_r,gaussian_term,dt):
    
    new_values = (math.exp(-theta*dt) * np.ravel(initial_values)) + mu_r + (sigma_r * np.ravel(gaussian_term))
    
    return new_values[:,np.newaxis]
    
sigma_squared_func = lambda theta, sigma, dt : (1.0 - math.exp(-2.0*theta*dt)) * sigma**2 / (2.0 * theta)
mu_func = lambda mu, theta, dt :  mu * (1.0 - math.exp(-theta*dt))

def generatePathsOU(initial_value,theta,mu,sigma,dt,num_time_steps,num_of_paths):
    
    sample_paths = initial_value * np.ones((num_of_paths,1))
    
    sigma_r = sigma_squared_func(theta,sigma,dt) ** 0.5
    
    mu_r = mu_func(mu,theta,dt)
    
    for i in xrange(num_time_steps):
    
        rand_vect = np.random.normal(0.0,1.0,(num_of_paths,1))
    
        sample_paths = np.hstack([sample_paths,oneStepExactSimulationOU(sample_paths[:,i],mu_r,theta,sigma_r,rand_vect,dt)])
    
    return sample_paths
    
def generatePathsOUAntiThetic(initial_value,theta,mu,sigma,dt,num_time_steps,num_of_paths):
    
    if num_of_paths % 2 != 0:
        num_of_paths += 1
    
    sample_paths = initial_value * np.ones((num_of_paths,1))
    
    sigma_r = sigma_squared_func(theta,sigma,dt) ** 0.5
    
    mu_r = mu_func(mu,theta,dt)
    
    for i in xrange(num_time_steps):
    
        rand_vect = np.random.normal(0.0,1.0,(num_of_paths/2,1))
        
        rand_vect = np.vstack([rand_vect,-1.0 * rand_vect])
    
        sample_paths = np.hstack([sample_paths,oneStepExactSimulationOU(sample_paths[:,i],mu_r,theta,sigma_r,rand_vect,dt)])
    
    return sample_paths

# Exact simulation of stationary OU process Glasserman p.111
def generateStationaryPathsOUAntiThetic(theta,mu,sigma,dt,num_time_steps,num_of_paths):
    
    if num_of_paths % 2 != 0:
        num_of_paths += 1
        
    stationary_std_dev = ((sigma**2)/(2.0 * theta))**0.5
    
    initial_values = np.random.normal(mu,stationary_std_dev,(num_of_paths/2,1))
    
    sample_paths = np.vstack([initial_values,-1.0 * initial_values])
    
    sigma_r = sigma_squared_func(theta,sigma,dt) ** 0.5
    
    mu_r = mu_func(mu,theta,dt)
    
    for i in xrange(num_time_steps):
    
        rand_vect = np.random.normal(0.0,1.0,(num_of_paths/2,1))
        
        rand_vect = np.vstack([rand_vect,-1.0 * rand_vect])
    
        sample_paths = np.hstack([sample_paths,oneStepExactSimulationOU(sample_paths[:,i],mu_r,theta,sigma_r,rand_vect,dt)])
    
    return sample_paths
    
diffusion_term = lambda mean_norm,std_dev_norm,gaussian_term : mean_norm + (std_dev_norm * gaussian_term)

def jump_term_exp_aux(count, scale): 
    if count > 0:
        return np.sum(np.random.exponential(scale,count))
    return 0.0

jump_term_exp = lambda scale, count_term: np.apply_along_axis(jump_term_exp_aux,1,count_term,scale)
    
# Exact simulation of jump diffusion process (Glasserman p. 138--139)
def oneStepExactSimulationJD(initial_values,mean_norm,std_dev_norm,scale,count_term,gaussian_term):
    
    new_values = np.ravel(initial_values) + np.ravel(diffusion_term(mean_norm,std_dev_norm,gaussian_term)) + np.ravel(jump_term_exp(scale, count_term))
    
    return new_values[:,np.newaxis]

def generatePathsJD(initial_value,mu_norm,sigma_norm,jump_rate,exp_rate,dt,num_time_steps,num_of_paths):
    
    sample_paths = initial_value * np.ones((num_of_paths,1))
    
    mean_norm = mu_norm * dt
    
    std_dev_norm = sigma_norm * (dt**0.5)
    
    scale = 1.0 / exp_rate
    
    for i in xrange(num_time_steps):
        
        # generate Poisson random variables
        count_term = np.random.poisson(jump_rate*dt,(num_of_paths,1))
        gaussian_term = np.random.normal(0.0,1.0,(num_of_paths,1))
    
        sample_paths = np.hstack([sample_paths,oneStepExactSimulationJD(sample_paths[:,i],mean_norm,std_dev_norm,scale,count_term,gaussian_term)])
    
    return sample_paths