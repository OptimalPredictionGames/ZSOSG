"""
import numpy as np
import time
import operator
import pylab as plt
"""

from common_imports import *
import copy, multiprocessing, itertools
from textwrap import wrap
cimport cython
from libc.math cimport exp

# lower and upper payoffs of the game
def lower_payoff(float price, float strike, bint doPutOption):
    cdef float payoff
    if doPutOption:
        # put option payoff
        payoff = max(strike-price, 0.0)
    else:
        # call option payoff
        payoff = max(price-strike, 0.0)
    return payoff
    
def upper_payoff(float price, float strike, float delta, bint doPutOption):
    cdef float payoff = lower_payoff(price, strike, doPutOption) + delta
    return payoff

# terminal reward function
def TerminalReward(float price, float strike, float current_control, bint doPutOption):
    cdef float reward = lower_payoff(price, strike, doPutOption) * current_control
    return reward
    
"""
Basic requirements:
switchingCost must satisfy a ``triangle inequality'':
SC(i,j) < SC(i,k) + SC(k,j) for any i,j,k where (i != j) and (j != k)
Also SC(i,i) = 0

"""
# @param string $initial_control : initial choice of control, either 0.0 or 1.0
# @param float $control_choice : new choice of control, either 0.0 or 1.0
# @return float $switching_cost : return the cost of switching

def switchingCost(float initial_control, float control_choice, float price, float strike, float delta, bint doPutOption):

    if (initial_control == control_choice):
        return 0.0
        
    cdef float switching_cost = (upper_payoff(price,strike,delta,doPutOption) * control_choice) - (lower_payoff(price,strike,doPutOption) * (1.0 - control_choice))
    
    return switching_cost
    
# get the coefficients for the basis functions    
def getCoefficients(regressand,regressors):
    coefficients = np.linalg.lstsq(regressors,regressand)[0][:,np.newaxis]
    return coefficients

# get the expected value of the (future) next stage costs given the information available in this stage (present) and switching costs
def doGetExpectedCosts(int time_pos, float initial_control, float control_choice, sample_paths, regressors, quasiValueFuncs,
                       float dt, int num_sample_paths, float interest_rate, float strike, float delta, bint doPutOption):
    # get the costs based on this choice of control
    
    expected_decision_costs = ''
    decision_costs =  exp(-interest_rate*dt) * quasiValueFuncs[control_choice]

    # estimate the conditional expectation of the next stage costs if num_sample_paths > 1
    # see Carmona and Ludkovski (2010), pg. 365
        
    if (num_sample_paths > 1):
        if (time_pos > 0):
            # get the coefficients for the polynomial basis functions to estimate the conditional expectations
            # using linear (OLS) regression
            coefficients = getCoefficients(decision_costs,regressors)
            # expected_decision_costs are the fitted values from the regression
            # referred to as quasi-value functions by Carmona and Ludkovski (2010)
            expected_decision_costs = np.ravel(regressors*coefficients)
        else:
            expected_decision_cost = np.mean(decision_costs)
            expected_decision_costs = np.empty_like(decision_costs)
            expected_decision_costs.fill(expected_decision_cost)
    else:
        expected_decision_costs = np.empty_like(decision_costs)
        expected_decision_costs[:] = decision_costs
        
    switching_costs = np.array([switchingCost(initial_control,control_choice,sample_paths[l,time_pos],strike,delta,doPutOption)
                        for l in xrange(num_sample_paths)])
        
    return {'estimator' : expected_decision_costs - switching_costs, 'future value' : decision_costs - switching_costs}
    
    
# get regressors for estimating conditional expectation of value function

def getRegressionVariables(random_variates, int degree):
    # https://docs.scipy.org/doc/numpy/reference/routines.polynomials.package.html
    regressors = np.asmatrix(np.polynomial.polynomial.polyvander(random_variates,degree))
    return regressors
    
"""
Based on either the Longstaff-Schwartz or TvR version

p. 461 of Glasserman (2004)

@param int $this_path : the sample path

"""
    
def doGetOptimalDecision(float initial_control, control_expected_costs, int this_path, bint useLS):
    # initialise to continuation value
    cdef float optimal_decision_rule = initial_control
    cdef float optimal_cost = control_expected_costs[initial_control]['estimator'][this_path]
    cdef float this_control
    
    keys_list = control_expected_costs.keys()
    keys_list.remove(initial_control)
    
    # get the maximum value of the quasi-value functions and the associated control along each path
    for this_control in keys_list:
        if (control_expected_costs[this_control]['estimator'][this_path] > optimal_cost):
            # perform switch
            optimal_decision_rule = this_control
            optimal_cost = control_expected_costs[this_control]['estimator'][this_path]

    if useLS:
        # LS (also see pg. 425 of Carmona and Ludkovski (2008))
        optimal_cost = control_expected_costs[optimal_decision_rule]['future value'][this_path]

    return (optimal_decision_rule, optimal_cost)
    
# determine the optimal action

def doOptimalDecisionCalculation(int time_pos, sample_paths, float initial_control, controls, quasiValueFuncsOld, int degree,
    float dt, float interest_rate, float strike, float delta, bint doPutOption, bint useLS):
    
    num_sample_paths = sample_paths.shape[0]
    
    regressors = getRegressionVariables(np.ravel(sample_paths[:,time_pos]),degree)
    
    # given the current state of the system (time,current control) find the new value of the control
    # expected gain given a penalty for switching from the current control...
        
    control_expected_costs = {control_choice : doGetExpectedCosts(time_pos, initial_control, control_choice, sample_paths, regressors, quasiValueFuncsOld,\
        dt, num_sample_paths, interest_rate, strike, delta, doPutOption) for control_choice in controls}
        
    # get the maxmimum value of the quasi-value functions and the associated control along each path
    
    sorted_controls_expected_values = [doGetOptimalDecision(initial_control,control_expected_costs,this_path,useLS) for this_path in xrange(num_sample_paths)]
    
    # recall that sorted_controls_expected_values = (control_choice,expected_cost)
    controls, expected_values = np.array(zip(*sorted_controls_expected_values))

    return (initial_control, (controls, expected_values))

# @param list $controls : [0.0,1.0]
# @param integer $degree : the degree / order of the regression polynomial

def OptimalSwitchingControlGenerator(sample_paths, controls, float dt, int degree, float interest_rate, float strike,
                                     float delta, bint doPutOption = True, bint useLS = True):

    cdef float this_time
    cdef int num_sample_paths = sample_paths.shape[0]
    cdef int time_pos
    cdef int mode_counter
    
    # number of time iterations
    cdef int iterations = sample_paths.shape[1]
    
    # calculate terminal values
    quasiValueFuncs = {control_choice : np.array([TerminalReward(sample_paths[l,-1],strike,control_choice,doPutOption) for l in xrange(num_sample_paths)]) for control_choice in controls}
    
    # excludes the time 0 as all sample paths start from the same value
    # loop between (0,T) backwards
    for time_pos in range(iterations - 2,-1,-1):
        
        these_paths = sample_paths[:,time_pos]
        this_time = dt * time_pos
    
        # calculate the value function, optimal commands, etc. for each value of the initial_control
        #timer = time.time()
        
        optimisationResults = [doOptimalDecisionCalculation(time_pos, sample_paths, initial_control, controls, quasiValueFuncs,\
                degree, dt, interest_rate, strike, delta, doPutOption, useLS) for initial_control in controls]
        
        #timer = time.time() - timer
        
        #print "%f seconds taken to compute optimal decisions and quasi-value functions for %d sample paths" % (timer, num_sample_paths)

        for mode_counter in xrange(len(controls)):
            
            resultsByInitialControl = optimisationResults[mode_counter]
            
            initial_control = resultsByInitialControl[0]
            
            # get results for the new control mode and expected cost            
            controlPoliciesResults, quasiValueFuncsResults = resultsByInitialControl[1][0], resultsByInitialControl[1][1]
            
            quasiValueFuncs[initial_control][:] = quasiValueFuncsResults
        
    # now at time_pos = 0
        
    # now we find the maximising value of the control for each initial_control
    
    # first find the average over the paths
    
    valueFunction = {control_choice : np.mean(quasiValueFuncs[control_choice]) for control_choice in controls}
    
    return valueFunction