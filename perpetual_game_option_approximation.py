"""
import numpy as np
import math
import pylab as plt

"""
# python setup.py build_ext --inplace

from common_imports import *
import sample_path_routines
import time

def solveDynkinGame(sample_paths, OS_DGS, control_choices, step_size, degree, interest_rate, strike, delta, doPutOption, useLS):

    valueFunction = OS_DGS.OptimalSwitchingControlGenerator(sample_paths, control_choices, step_size, degree,
                                                         interest_rate, strike, delta, doPutOption, useLS)
    
    return (valueFunction[1.0], valueFunction[0.0])

if __name__ == '__main__':

    import dynkin_games_kifer as YK_DGS
    import perpetual_american_put_option as PAPO
    import perpetual_american_call_option as PACO

    doPutOption = True
    doPerpetual = True
    doFiniteHorizon = False
    
    # Callable put option : Kuhn et al "Pricing Israeli options: a pathwise approach", p. 128
    interest_rate = 0.06
    volatility = 0.4
    dividend_rate = 0.0
    delta = 5.0
    strike = 100.0
    initial_time = 0.0
    kifer_header = ''
    initial_stock_prices = [60.0, 80.0, 100.0, 120.0, 140.0]
    use_indices = [0,1]

    if doFiniteHorizon:
        output_folder = './results/put_option_no_dividends/final/'
        degree = 2
        useLS = True
        # 100
        num_simulations = 1
        # 10000 and 200
        num_of_paths = 50000
        num_time_steps = 1000

        results_header = "Initial Stock Price" + ',' + "Terminal Time" ',' + "Mean V(1)" + ',' + "SD V(1)" + ',' + "Mean V(0)" + ',' \
                         + "SD V(0)" + ',' + "Mean Game" + ',' + "SD Game" + '\n'
        kifer_header = "Initial Stock Price" + ',' + "Terminal Time" ',' + "Mean Game" + ',' + "SD Game" + '\n'
        terminal_time_arr = [0.5, 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0]
    for this_index in use_indices:
        initial_stock_price = initial_stock_prices[this_index]
        print "initial asset price is %f" % initial_stock_price
        if doPerpetual:
            if doPutOption:
                value_perpetual_option = PAPO.getDeltaPerpetualAmericanPutOptionValue(interest_rate, volatility, strike, delta, initial_stock_price)
                print "The value of the perpetual delta-callable put option for delta = %f and initial asset price %f is %f" % (delta, initial_stock_price, value_perpetual_option)
            else:
                value_perpetual_option = PACO.getValuePerpetualGameCallOptionWithoutDividends(initial_stock_price, strike, delta)
                print "The value of the perpetual game call option without dividends for delta = %f and initial asset price %f is %f" % (delta, initial_stock_price, value_perpetual_option)
        if doFiniteHorizon:
            all_game_results = []
            results_string = ''
            for terminal_time in terminal_time_arr:
                print "terminal time is %f" % terminal_time
                step_size = (terminal_time - initial_time) / float(num_time_steps)
                mean_norm = (interest_rate - dividend_rate - (0.5 * volatility * volatility)) * step_size
                std_dev_norm = volatility * (step_size**0.5)
                results_arr = []
                timer = time.time()
                sim_counter = 1
                for repeat_this in xrange(num_simulations):
                    sample_paths = sample_path_routines.generatePathsGBMAntiThetic(initial_stock_price, mean_norm, std_dev_norm, num_time_steps, num_of_paths)
                    results_arr.append(YK_DGS.doDynkinGameValuation(sample_paths, step_size, degree, interest_rate,
                                                                    strike, delta, doPutOption, useLS))
                    print "Just finished simulation %d of %d" % (sim_counter, num_simulations)
                    sim_counter += 1
                timer = time.time() - timer

                print "%d simulations with %f sample paths took %f seconds in total, which is %f seconds per simulation" % (num_simulations,num_of_paths,round(timer,3),round(timer/num_simulations,3))

                game_results = np.array(results_arr)
                mean_game = np.mean(game_results)

                all_game_results.append(mean_game)

                std_dev_game = np.std(game_results, ddof=1)

                results_string = results_string + str(initial_stock_price) + ',' + str(terminal_time) + ',' + \
                                 str(mean_game) + ',' + str(std_dev_game) + '\n'

            f_handle = open(output_folder+'TvR_kifer_simulation_results_'+str(this_index+1)+'.csv','w')
            f_handle.write(kifer_header+results_string)
            f_handle.close()

            plt.figure()
            #plt.title('Comparison of (mean simulated) finite expiry and (analytical) perpetual \n option values for ${s = '+str(initial_stock_price)+'}$\n')
            plt.xlim(xmin=terminal_time_arr[0],xmax=terminal_time_arr[-1])
            plt.ylim(ymin=0.95*min(min(all_game_results), value_perpetual_option),ymax=1.05*max(max(all_game_results),value_perpetual_option))
            plt.xlabel('Finite Horizon Terminal Time - T')
            plt.ylabel('Option Value')
            plt.tight_layout()

            ax = plt.subplot(111)
            ax.plot(terminal_time_arr,all_game_results,label='finite horizon')
            if doPerpetual:
                ax.axhline(value_perpetual_option, label='perpetual', linestyle='--', color='r')
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            plt.savefig(output_folder+"TvR_kifer_results_"+str(this_index+1)+".png",bbox_inches='tight')
