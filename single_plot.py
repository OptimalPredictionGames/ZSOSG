__author__ = 'rmartyr'

# python setup.py build_ext --inplace

from common_imports import *


def plotOptionPrices(initial_stock_prices, mean_values, plot_every, file_folder, figName, **kwargs):

    fig, ax1 = plt.subplots()

    ax1.yaxis.grid(linestyle='-')

    markers = ["*","x","o",'d','>']
    colours = ['b','r','c','m','k']

    for count in xrange(len(mean_values)):
        x_arr, label = mean_values[count]
        marker = markers[count]
        colour = colours[count]
        ax1.plot(initial_stock_prices, x_arr, color=colour, linewidth=1.5, marker=marker, label=label,
                 markevery=max(len(x_arr[::plot_every]), 1)
                 )

    ax1.set_xlabel('Initial Asset Price')
    ax1.set_ylabel('Option Value')

        # Shink current axis by 20%
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0, box.width * 0.95, box.height])

    ax1.legend(loc='lower left', bbox_to_anchor=(1.15, 0.4))

    plt.savefig(file_folder+figName, bbox_inches='tight')

    plt.close()


if __name__ == '__main__':

    file_folder = "./"
    labels = []
    mean_values = []
    initial_stock_prices = np.arange(80.0, 120.1, 10.0)
    plot_every = (max(initial_stock_prices) - min(initial_stock_prices)) / 2

    T_values = [0.5, 1.0, 2.0, 4.0, 8.0]

    for count in xrange(len(T_values)):
        file_name = 'simulation_results_'+str(count + 1)+'.csv'
        if os.path.isfile(file_folder+file_name):
            # get the data
            results = np.loadtxt(file_folder+file_name, delimiter=',', skiprows=1, usecols=(5,))
            mean_values.append((results.ravel(),'${T = '+str(T_values[count])+'}$'))

    figName = "option_values_different_horizon.png"
    plotOptionPrices(initial_stock_prices, mean_values, plot_every, file_folder, figName)