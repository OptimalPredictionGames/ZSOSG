"""
Uses least-squares Monte carlo and the discrete-time solution to Israeli option in Theorem 2.1 of Kifer (2000)
Kifer, Y. (2000). "Game options". Finance and Stochastics, 4(4), pp. 443-463.

import numpy as np
import time
import operator
import pylab as plt
"""

from common_imports import *
import copy, multiprocessing, itertools
from textwrap import wrap
cimport cython
from libc.math cimport exp

# lower and upper payoffs of the game
def lower_payoff(float price, float strike, bint doPutOption):
    cdef float payoff
    if doPutOption:
        # put option payoff
        payoff = max(strike-price, 0.0)
    else:
        # call option payoff
        payoff = max(price-strike, 0.0)
    return payoff

def upper_payoff(float price, float strike, float delta, bint doPutOption):
    cdef float payoff = lower_payoff(price, strike, doPutOption) + delta
    return payoff

# terminal reward function
def TerminalReward(float price, float strike, bint doPutOption):
    return lower_payoff(price, strike, doPutOption)

# get the coefficients for the basis functions    
def getCoefficients(regressand,regressors):
    coefficients = np.linalg.lstsq(regressors,regressand)[0][:,np.newaxis]
    return coefficients

# get the expected value of the (future) next stage costs given the information available in this stage (present) and switching costs
def doGetExpectedCosts(int time_pos, regressors, quasiValueFuncs, float dt, int num_sample_paths,
                       float interest_rate, float strike, float delta, bint doPutOption):

    # get the costs based on this choice of control
    
    expected_decision_costs = ''
    decision_costs =  exp(-interest_rate*dt) * quasiValueFuncs

    # estimate the conditional expectation of the next stage costs if num_sample_paths > 1
    # see Carmona and Ludkovski (2010), pg. 365
        
    if (num_sample_paths > 1):
        if (time_pos > 0):
            # get the coefficients for the polynomial basis functions to estimate the conditional expectations
            # using linear (OLS) regression
            coefficients = getCoefficients(decision_costs,regressors)
            # expected_decision_costs are the fitted values from the regression
            # referred to as quasi-value functions by Carmona and Ludkovski (2010)
            expected_decision_costs = np.ravel(regressors*coefficients)
        else:
            expected_decision_cost = np.mean(decision_costs)
            expected_decision_costs = np.empty_like(decision_costs)
            expected_decision_costs.fill(expected_decision_cost)
    else:
        expected_decision_costs = np.empty_like(decision_costs)
        expected_decision_costs[:] = decision_costs
        
    return {'estimator' : expected_decision_costs, 'future value' : decision_costs}
    
    
# get regressors for estimating conditional expectation of value function

def getRegressionVariables(random_variates, int degree):
    # https://docs.scipy.org/doc/numpy/reference/routines.polynomials.package.html
    regressors = np.asmatrix(np.polynomial.polynomial.polyvander(random_variates,degree))
    return regressors
    
"""
Based on either the Longstaff-Schwartz or TvR version

p. 461 of Glasserman (2004)

@param int $this_path : the sample path

"""
    
def doGetValueFunction(expected_costs, int this_path, float price, float strike, float delta, bint doPutOption,
                       bint useLS):

    cdef float lower_payoff_value = lower_payoff(price, strike, doPutOption)
    cdef float upper_payoff_value = lower_payoff_value + delta

    # initialise to stopping value
    cdef float optimal_cost = lower_payoff_value

    # optimal stopping 'sup' problem
    if (expected_costs['estimator'][this_path] > optimal_cost):
        if useLS:
            optimal_cost = expected_costs['future value'][this_path]
        else:
            optimal_cost = expected_costs['estimator'][this_path]

    # optimal stopping 'inf' problem
    optimal_cost = min(optimal_cost, upper_payoff_value)

    return optimal_cost
    
# determine the optimal action

def doOptimalDecisionCalculation(int time_pos, sample_paths, quasiValueFuncsOld, int degree, float dt,
                                 float interest_rate, float strike, float delta, bint doPutOption, bint useLS):

    num_sample_paths = sample_paths.shape[0]
    
    regressors = getRegressionVariables(np.ravel(sample_paths[:,time_pos]),degree)
        
    expected_costs = doGetExpectedCosts(time_pos, regressors, quasiValueFuncsOld, dt, num_sample_paths, interest_rate,
                                        strike, delta, doPutOption)
    
    expected_values = np.array([
                                doGetValueFunction(expected_costs,this_path,sample_paths[this_path,time_pos], strike,
                                                   delta, doPutOption, useLS)
                                for this_path in xrange(num_sample_paths)])

    return expected_values

# @param list $controls : [0.0,1.0]
# @param integer $degree : the degree / order of the regression polynomial

def doDynkinGameValuation(sample_paths, float dt, int degree, float interest_rate, float strike, float delta,
                                     bint doPutOption = True, bint useLS = True):

    cdef float this_time
    cdef int num_sample_paths = sample_paths.shape[0]
    cdef int time_pos
    
    # number of time iterations
    cdef int iterations = sample_paths.shape[1]
    
    # calculate terminal values
    quasiValueFuncs = np.array([TerminalReward(sample_paths[l,-1],strike,doPutOption) for l in xrange(num_sample_paths)])
    
    # excludes the time 0 as all sample paths start from the same value
    # loop from T to 0
    for time_pos in range(iterations - 2,-1,-1):
        
        these_paths = sample_paths[:,time_pos]
        this_time = dt * time_pos
    
        # calculate the value function, optimal commands, etc. for each value of the initial_control
        #timer = time.time()
        
        quasiValueFuncs[:] = doOptimalDecisionCalculation(time_pos, sample_paths, quasiValueFuncs, degree, dt,
                                                            interest_rate, strike, delta, doPutOption, useLS)
        
        #timer = time.time() - timer
        
        #print "%f seconds taken to compute optimal decisions and quasi-value functions for %d sample paths" % (timer, num_sample_paths)
        
    # now at time_pos = 0
        
    # now we find the maximising value of the control for each initial_control
    
    # first find the average over the paths
    
    game_value = np.mean(quasiValueFuncs)
    
    return game_value