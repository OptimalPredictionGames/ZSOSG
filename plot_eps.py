from common_imports import *

if __name__ == '__main__':

    doPutOption = True

    if doPutOption:
        import perpetual_american_put_option as PAPO
    else:
        import perpetual_american_call_option as PACO


    # Callable put option : Kuhn et al "Pricing Israeli options: a pathwise approach", p. 128
    interest_rate = 0.06
    volatility = 0.4
    dividend_rate = 0.0
    delta = 5.0
    strike = 100.0

    input_folder = './input/'

    if doPutOption:
        input_folder += 'put option/'
    else:
        input_folder += 'call option/'

    f_name_start = "TvR_kifer_simulation_results_"

    use_indices = [0,4]
    char_arr = ['a','b']

    for count in xrange(len(use_indices)):
        index = use_indices[count]
        read_data = np.loadtxt(input_folder+f_name_start+str(index + 1)+'.csv',delimiter=',',skiprows=1,usecols=(0,1,2))
        initial_stock_price, terminal_time_arr, all_game_results = np.hsplit(read_data, 3)
        initial_stock_price = int(initial_stock_price[0])

        if doPutOption:
            value_perpetual_option = PAPO.getDeltaPerpetualAmericanPutOptionValue(interest_rate, volatility, strike, delta, initial_stock_price)
            print "The value of the perpetual delta-callable put option for delta = %f and initial asset price %f is %f" % (delta, initial_stock_price, value_perpetual_option)
        else:
            value_perpetual_option = PACO.getValuePerpetualGameCallOptionWithoutDividends(initial_stock_price, strike, delta)
            print "The value of the perpetual game call option without dividends for delta = %f and initial asset price %f is %f" % (delta, initial_stock_price, value_perpetual_option)

        plt.figure()
        title = 'Numerical results for cancellable '
        if doPutOption:
            title += 'put'
        else:
            title += 'call'
        title += ' option: ${S_{0} = '+str(initial_stock_price)+'}$\n'
        plt.title(title)
        plt.xlim(xmin=terminal_time_arr[0],xmax=terminal_time_arr[-1])
        plt.ylim(ymin=0.95*min(min(all_game_results), value_perpetual_option),ymax=1.05*max(max(all_game_results),value_perpetual_option))
        plt.xlabel('Finite Horizon Terminal Time - T')
        plt.ylabel('Option Value')
        plt.tight_layout()

        ax = plt.subplot(111)
        ax.plot(terminal_time_arr,all_game_results,label='finite horizon')
        ax.axhline(value_perpetual_option, label='perpetual', linestyle='--', color='r')
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        if doPutOption:
            plt.savefig(input_folder+"figure_2_"+char_arr[count]+".eps", format='eps', dpi=1000, bbox_inches='tight')
        else:
            plt.savefig(input_folder+"figure_1_"+char_arr[count]+".eps", format='eps', dpi=1000, bbox_inches='tight')